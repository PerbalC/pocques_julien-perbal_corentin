﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterClassLibrary
{

    public class Compteur
    {
        private static int compte = 0;

        public static int getCompte()   {return compte;}

        public static void setCompte(int newcompte) { compte = newcompte; }

        public static int incrementation ()
        {
            Compteur.setCompte(Compteur.getCompte() + 1);
            return compte;
        }
        public static int decrementation ()
        {
            Compteur.setCompte(Compteur.getCompte() - 1);
            return compte;
        }
        public static int remise_a_zero()
        {
            Compteur.setCompte(0);
            return compte;
        }
    }
}
