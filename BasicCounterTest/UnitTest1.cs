﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterClassLibrary;
namespace BasicCounterTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestGetCompte()
        {
            int compte = 0;
            Assert.AreEqual(0, Compteur.getCompte());
            compte = 5;
            Assert.AreEqual(5, Compteur.getCompte());
        }
        [TestMethod]
        public void TestIncrementation()
        {
            int compte = 0;
            Assert.AreEqual(1, Compteur.incrementation());
            compte = 5;
            Assert.AreEqual(6, Compteur.incrementation());
        }
        [TestMethod]
        public void TestDecrementation()
        {
            int compte = 1;
            Assert.AreEqual(0, Compteur.decrementation());
            compte = 5;
            Assert.AreEqual(4, Compteur.decrementation());
        }
        [TestMethod]
        public void TestRAZ()
        {
            int compte = 98;
            Assert.AreEqual(0, Compteur.remise_a_zero());
            compte = 5;
            Assert.AreEqual(0, Compteur.remise_a_zero());
        }
    }
}
